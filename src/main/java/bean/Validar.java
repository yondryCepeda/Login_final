package bean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import main.Login;


@ManagedBean
@SessionScoped
public class Validar {
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");
	EntityManager em = emf.createEntityManager();
	
	private String nombre;
	private String pass;
	List<Login> lista =new ArrayList();
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	
	
	public String comprobar(String nom, String passw){
		
		String result ="";
	
			lista = em.createNamedQuery("Login.ListaUsuarios", Login.class)
					.setParameter("user", nom)
					.setParameter("password", passw)
					.getResultList();
			
		System.out.println("lista de personas"+lista);
		 
		if(lista.isEmpty()) {
			return "Invalido";
		}
		else {
			return "Bienvenido";
		}
		
		
		//165732 numero de reporte
		
	}
}
